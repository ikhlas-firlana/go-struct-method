package main

import (
	"fmt"
	"time"
)

func main() {
	whoareyou := person{}
	// set my name
	whoareyou.Name = "Ikhlas Firlana"
	// now we set location
	loc, _ := time.LoadLocation("Asia/Jakarta")
	whoareyou.Birth = time.Date(1989, 11, 11, 0, 0, 0, 0, loc)

	fmt.Println("Name ", whoareyou.Name)
	fmt.Println("Age ", whoareyou.age())

}

type person struct {
	Name  string
	Birth time.Time
}

func (p person) age() int {
	return Age(p.Birth)
}

// AgeAt package from go-age
// lines below i got from https://github.com/bearbin/go-age
// thank Alexander Harkness
func AgeAt(birthDate time.Time, now time.Time) int {
	// Get the year number change since the player's birth.
	years := now.Year() - birthDate.Year()

	// If the date is before the date of birth, then not that many years have elapsed.
	birthDay := getAdjustedBirthDay(birthDate, now)
	if now.YearDay() < birthDay {
		years--
	}

	return years
}

// Age is shorthand for AgeAt(birthDate, time.Now()), and carries the same usage and limitations.
func Age(birthDate time.Time) int {
	return AgeAt(birthDate, time.Now())
}

// Gets the adjusted date of birth to work around leap year differences.
func getAdjustedBirthDay(birthDate time.Time, now time.Time) int {
	birthDay := birthDate.YearDay()
	currentDay := now.YearDay()
	if isLeap(birthDate) && !isLeap(now) && birthDay >= 60 {
		return birthDay - 1
	}
	if isLeap(now) && !isLeap(birthDate) && currentDay >= 60 {
		return birthDay + 1
	}
	return birthDay
}

// Works out if a time.Time is in a leap year.
func isLeap(date time.Time) bool {
	year := date.Year()
	if year%400 == 0 {
		return true
	} else if year%100 == 0 {
		return false
	} else if year%4 == 0 {
		return true
	}
	return false
}
